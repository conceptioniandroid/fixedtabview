package com.conceptioni.tabviewdemo;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class Tabview extends AppCompatActivity {

    TabLayout tabview;
    private ViewPager viewpager;

    private int[] tabnames = {R.string.tab1,
            R.string.tab2,
            R.string.tab3,
            R.string.tab4,
            R.string.tab5,
            R.string.tab6,
            R.string.tab7,
            R.string.tab8
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tabview);

        viewpager = findViewById(R.id.viewpager);
        tabview = findViewById(R.id.tabview);

        ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        if (viewpager != null) {
            viewpager.setAdapter(pagerAdapter);
        }
        if (tabview != null) {
            tabview.setupWithViewPager(viewpager);
            for (int i = 0; i < tabview.getTabCount(); i++) {
                TabLayout.Tab tab = tabview.getTabAt(i);
                if (tab != null)
                    tab.setCustomView(pagerAdapter.getTabView(i));
            }
            tabview.getTabAt(0).getCustomView().setSelected(true);
        }

    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {

        final int PAGE_COUNT = 4;

        ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        View getTabView(int position) {
            @SuppressLint("InflateParams") View view = LayoutInflater.from(Tabview.this).inflate(R.layout.custom_home_tab, null);
            TextView tabnametvr = view.findViewById(R.id.title);
            tabnametvr.setText(tabnames[position]);

            return view;
        }

        @Nullable
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new Fragment1();
                case 1:
                    return new Fragment2();
                case 2:
                    return new Fragment3();
                case 3:
                    return new Fragment4();



            }
            return null;
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return super.getItemPosition(object);
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public void finishUpdate(ViewGroup container) {
            try {
                super.finishUpdate(container);
            } catch (NullPointerException nullPointerException) {
                System.out.println("Catch the NullPointerException in FragmentPagerAdapter.finishUpdate");
            }
        }
    }

}
